﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Exe.MyShows.Api.Models;

namespace Exe.MyShows.Api
{
	public interface IMyShowsApi
	{
		IMyShowsSession Session { get; }

		Task<List<Episode>> GetEpisodesAsync(WatchState state);
	}
}
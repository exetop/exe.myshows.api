﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exe.MyShows.Api.Extensions;
using Exe.MyShows.Api.Models;
using Exe.MyShows.Api.Models.Dto;
using Exe.MyShows.Api.Resources;

namespace Exe.MyShows.Api
{
	public sealed class MyShowsApi : IMyShowsApi
	{
		public IMyShowsSession Session { get; private set; }

		private const string ApiUrl = "https://api.myshows.me/v2/rpc/";

		public MyShowsApi(IMyShowsSession session)
		{
			Session = session ?? throw new ArgumentNullException(nameof(session));
		}

		public async Task<List<Episode>> GetEpisodesAsync(WatchState watchState)
		{
			var data = await Session.RestClient.ExecuteApiRequestAsync<List<GetEpisodesDto>>(
				ApiUrl, Session.TokenInfo, new RestRequestParams
				{
					method = MyShowsApiMethod.ListsEpisodes,
					@params = new { list = watchState.ToString().ToLower() }
				});

			return data.Select(o =>
					   {
						   o.episode.show = o.show;
						   o.episode.watchState = watchState;
						   return o.episode;
					   })
					   .AutoMap<List<Episode>>();
		}

		[Serializable]
		private sealed class GetEpisodesDto
		{
			public EpisodeDto episode { get; set; }
			public ShowDto show { get; set; }
		}
	}
}
﻿using System.Threading.Tasks;
using Exe.MyShows.Api.Models;
using RestSharp;

namespace Exe.MyShows.Api
{
	public interface IMyShowsSession
	{
		TokenInfo TokenInfo { get; }
		IRestClient RestClient { get; }

		Task<TokenInfo> RequestTokenAsync(string username, string password);
		Task<TokenInfo> RefreshTokenAsync();
	}
}
﻿using System;
using System.Threading.Tasks;
using Exe.MyShows.Api.Extensions;
using Exe.MyShows.Api.Models;
using Exe.MyShows.Api.Resources;
using RestSharp;

namespace Exe.MyShows.Api
{
	public sealed class MyShowsSession : IMyShowsSession
	{
		public TokenInfo TokenInfo { get; private set; }
		public IRestClient RestClient { get; private set; }

		private const string OAuthBaseUrl = "https://myshows.me";
		private const string OAuthRoute = "/oauth/token";

		private readonly string _clientId;
		private readonly string _clientSecret;

		private readonly object _objectLock;

		public MyShowsSession(string clientId, string clientSecret)
			: this(clientId, clientSecret, tokenInfo: null)
		{
		}

		public MyShowsSession(string clientId, string clientSecret, TokenInfo tokenInfo)
		{
			_clientId = clientId ?? throw new ArgumentNullException(nameof(clientId));
			_clientSecret = clientSecret ?? throw new ArgumentNullException(nameof(clientSecret));
			_objectLock = new object();

			TokenInfo = tokenInfo;
			RestClient = new RestClient(OAuthBaseUrl);
		}

		public async Task<TokenInfo> RequestTokenAsync(string username, string password)
		{
			if (username.IsNullOrWhiteSpace())
				throw new ArgumentException(nameof(username));

			if (password.IsNullOrWhiteSpace())
				throw new ArgumentException(nameof(password));

			var newTokenInfo = await RestClient.ExecuteOAuthRequestAsync(OAuthRoute, new OAuthRequestTokenParams
			{
				ClientId = _clientId,
				ClientSecret = _clientSecret,
				Username = username,
				Password = password
			});

			lock (_objectLock)
				return TokenInfo = newTokenInfo;
		}

		public async Task<TokenInfo> RefreshTokenAsync()
		{
			if (TokenInfo == null)
				throw new InvalidOperationException(ErrorMessage.TokenNotSet);

			var newTokenInfo = await RestClient.ExecuteOAuthRequestAsync(OAuthRoute, new OAuthRefreshTokenParams
			{
				ClientId = _clientId,
				ClientSecret = _clientSecret,
				RefreshToken = TokenInfo.RefreshToken
			});

			lock (_objectLock)
				return TokenInfo = newTokenInfo;
		}
	}
}
﻿using System;

namespace Exe.MyShows.Api.Models
{
	[Serializable]
	public sealed class Show : IEquatable<Show>
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string TitleOriginal { get; set; }

		// todo: status

		public int SeasonsCount { get; set; }
		public string ImageUrl { get; set; }

		public override int GetHashCode()
		{
			var hasId = Id.GetHashCode();
			var hasTitle = Title?.GetHashCode() ?? 0;
			var hasTitleOriginal = TitleOriginal?.GetHashCode() ?? 0;
			var hasTotalSeasons = SeasonsCount.GetHashCode();
			var hasImageUrl = ImageUrl?.GetHashCode() ?? 0;

			return hasId ^ hasTitle ^ hasTitleOriginal ^ hasTotalSeasons ^ hasImageUrl;
		}

		bool IEquatable<Show>.Equals(Show other)
		{
			if (other == null)
				return false;

			if (other == this)
				return true;

			return Id == other.Id &&
				   Title == other.Title &&
				   TitleOriginal == other.TitleOriginal &&
				   SeasonsCount == other.SeasonsCount &&
				   ImageUrl == other.ImageUrl;
		}
	}
}
﻿using System;
using Newtonsoft.Json;

namespace Exe.MyShows.Api.Models.Dto
{
	[Serializable]
	internal sealed class EpisodeDto
	{
		public int id { get; set; }
		public string title { get; set; }
		public int episodeNumber { get; set; }
		public int seasonNumber { get; set; }
		public string shortName { get; set; }
		public string image { get; set; }
		public DateTime airDateUTC { get; set; }

		[JsonIgnore]
		public WatchState watchState { get; set; }

		[JsonIgnore]
		public ShowDto show { get; set; }
	}
}
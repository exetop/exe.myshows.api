﻿using System;

namespace Exe.MyShows.Api.Models.Dto
{
	[Serializable]
	internal sealed class TokenInfoDto
	{
		public string access_token { get; set; }
		public string refresh_token { get; set; }
		public TokenType token_type { get; set; }
		public int expires_in { get; set; }
	}
}
﻿using System;

namespace Exe.MyShows.Api.Models.Dto
{
	[Serializable]
	internal sealed class ShowDto
	{
		public int id { get; set; }
		public string title { get; set; }
		public string titleOriginal { get; set; }

		// todo: status

		public int totalSeasons { get; set; }
		public string image { get; set; }
	}
}
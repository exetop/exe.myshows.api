﻿using System;

namespace Exe.MyShows.Api.Models.Dto
{
	[Serializable]
	internal sealed class ErrorDto
	{
		public string error { get; set; }
		public string error_description { get; set; }
	}
}
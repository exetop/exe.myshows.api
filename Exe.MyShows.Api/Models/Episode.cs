﻿using System;
using Newtonsoft.Json;

namespace Exe.MyShows.Api.Models
{
	[Serializable]
	public sealed class Episode
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public int Number { get; set; }
		public int SeasonNumber { get; set; }
		public string ShortName { get; set; }
		public string ImageUrl { get; set; }
		public DateTime AirDateUtc { get; set; }
		public WatchState WatchState { get; set; }
		public Show Show { get; set; }

		[JsonIgnore]
		public DateTime AirDate => AirDateUtc.ToLocalTime();
	}
}
﻿using System;

namespace Exe.MyShows.Api.Models
{
	[Serializable]
	public enum WatchState
	{
		Unwatched = 1,
		Next = 2
	}
}
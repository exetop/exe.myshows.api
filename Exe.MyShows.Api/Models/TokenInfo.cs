﻿using System;
using Newtonsoft.Json;

namespace Exe.MyShows.Api.Models
{
	[Serializable]
	public sealed class TokenInfo
	{
		public string AccessToken { get; set; }

		public string RefreshToken { get; set; }

		public TokenType TokenType { get; set; }

		public int ExpiresInSeconds { get; set; }

		public DateTime RequestDateTimeUtc { get; set; }

		[JsonIgnore]
		public DateTime RequestDateTime => RequestDateTimeUtc.ToLocalTime();

		[JsonIgnore]
		public DateTime ExpireDateTime => ExpireDateTimeUtc.ToLocalTime();

		[JsonIgnore]
		public DateTime ExpireDateTimeUtc => RequestDateTimeUtc.AddSeconds(ExpiresInSeconds);

		[JsonIgnore]
		public bool IsExpired => ThisMomentUtc >= ExpireDateTimeUtc;

		[JsonIgnore]
		private DateTime ThisMomentUtc => DateTime.Now.ToUniversalTime();

		public TokenInfo()
		{
			RequestDateTimeUtc = ThisMomentUtc;
		}
	}
}
﻿using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Exe.MyShows.Api.Models;
using Exe.MyShows.Api.Models.Dto;
using Newtonsoft.Json;
using RestSharp;

namespace Exe.MyShows.Api.Extensions
{
	internal static class RestExtensions
	{
		public static async Task<TokenInfo> ExecuteOAuthRequestAsync(this IRestClient restClient, string url, IOAuthParams oauthParams)
		{
			return await restClient.ExecuteOAuthRequestAsync(url, oauthParams, CancellationToken.None);
		}

		public static async Task<TokenInfo> ExecuteOAuthRequestAsync(this IRestClient restClient,
			string url, IOAuthParams oauthParams, CancellationToken cancellationToken)
		{
			var request = PrepareOAuthRequest(url, oauthParams);
			var response = await restClient.ExecuteTaskAsync<TokenInfoDto>(request, cancellationToken);

			return response.ThrowIfNotSuccessful().Data.AutoMap<TokenInfo>();
		}

		public static async Task<IRestResponse> ExecuteApiRequestAsync(this IRestClient restClient,
			string url, TokenInfo tokenInfo, IRestParams restParams)
		{
			return await restClient.ExecuteApiRequestAsync(url, tokenInfo, restParams, CancellationToken.None);
		}

		public static async Task<IRestResponse> ExecuteApiRequestAsync(this IRestClient restClient,
			string url, TokenInfo tokenInfo, IRestParams restParams, CancellationToken cancellationToken)
		{
			var request = PrepareApiRequest(url, tokenInfo, restParams);
			var response = await restClient.ExecuteTaskAsync(request, cancellationToken);

			return response.ThrowIfNotSuccessful();
		}

		public static async Task<TResult> ExecuteApiRequestAsync<TResult>(this IRestClient restClient,
			string url, TokenInfo tokenInfo, IRestParams restParams) where TResult : class, new()
		{
			return await restClient.ExecuteApiRequestAsync<TResult>(url, tokenInfo, restParams, CancellationToken.None);
		}

		public static async Task<TResult> ExecuteApiRequestAsync<TResult>(this IRestClient restClient,
			string url, TokenInfo tokenInfo, IRestParams restParams, CancellationToken cancellationToken) where TResult : class, new()
		{
			var request = PrepareApiRequest(url, tokenInfo, restParams);
			var response = await restClient.ExecuteTaskAsync<RestResponseParams<TResult>>(request, cancellationToken);

			return response.ThrowIfNotSuccessful().Data.result;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static IRestRequest PrepareOAuthRequest(string oauthUrl, IOAuthParams oauthParams)
		{
			if (oauthUrl.IsNullOrEmpty())
				throw new ArgumentException(nameof(oauthUrl));

			if (oauthParams == null)
				throw new ArgumentNullException(nameof(oauthParams));

			return new RestRequest(oauthUrl, Method.POST)
				  .AddCommonHeaders()
				  .AddParameter("application/x-www-form-urlencoded", oauthParams, ParameterType.RequestBody);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static IRestRequest PrepareApiRequest(string apiUrl, TokenInfo tokenInfo, IRestParams restParams)
		{
			if (apiUrl.IsNullOrEmpty())
				throw new ArgumentException(nameof(apiUrl));

			if (tokenInfo == null)
				throw new ArgumentNullException(nameof(tokenInfo));

			if (restParams == null)
				throw new ArgumentNullException(nameof(restParams));

			return new RestRequest(apiUrl, Method.POST).AddCommonHeaders()
													   .AddHeader("authorization", $"{tokenInfo.TokenType} {tokenInfo.AccessToken}")
													   .AddJsonBody(restParams);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static IRestRequest AddCommonHeaders(this IRestRequest restRequest)
		{
			return restRequest.AddHeader("accept-language", "en")
							  .AddHeader("accept", "application/json")
							  .AddHeader("cache-control", "no-cache")
							  .AddHeader("content-type", "application/x-www-form-urlencoded");
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static IRestResponse ThrowIfNotSuccessful(this IRestResponse response)
		{
			if (response != null && !response.IsSuccessful)
			{
				var errorDto = JsonConvert.DeserializeObject<ErrorDto>(response.Content);
				var errorMessage = errorDto != null ? errorDto.error_description : response.ErrorMessage;

				var status = response.ResponseStatus;
				var statusCode = response.StatusCode;

				switch (response.StatusCode)
				{
					case HttpStatusCode.Unauthorized: throw new MyShowsUnauthorizedException(status, errorMessage);
					default:
						throw new MyShowsBadRequestException(status, statusCode, errorMessage);
				}
			}

			return response;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		private static IRestResponse<TResult> ThrowIfNotSuccessful<TResult>(this IRestResponse<TResult> response) where TResult : class, new()
		{
			ThrowIfNotSuccessful(response as IRestResponse);
			return response;
		}
	}

	#region OAuthParams

	internal interface IOAuthParams
	{
		string ClientId { get; set; }
		string ClientSecret { get; set; }
		string GrantType { get; }
	}

	[Serializable]
	internal abstract class OAuthParamsBase : IOAuthParams
	{
		public string ClientId { get; set; }
		public string ClientSecret { get; set; }

		public abstract string GrantType { get; }

		public override string ToString()
		{
			return $"grant_type={GrantType}&client_id={ClientId}&client_secret={ClientSecret}";
		}
	}

	[Serializable]
	internal sealed class OAuthRequestTokenParams : OAuthParamsBase
	{
		public string Username { get; set; }
		public string Password { get; set; }

		public override string GrantType => "password";

		public override string ToString()
		{
			return $"{base.ToString()}&username={Username}&password={Password}";
		}
	}

	[Serializable]
	internal sealed class OAuthRefreshTokenParams : OAuthParamsBase
	{
		public string RefreshToken { get; set; }

		public override string GrantType => "refresh_token";

		public override string ToString()
		{
			return $"{base.ToString()}&refresh_token={RefreshToken}";
		}
	}

	#endregion OAuthParams

	#region RestParams

	internal interface IRestParams
	{
		int id { get; }
		string jsonrpc { get; }
	}

	[Serializable]
	internal abstract class RestParamsBase : IRestParams
	{
		public int id => 1;
		public string jsonrpc => "2.0";
	}

	[Serializable]
	internal sealed class RestRequestParams : RestParamsBase
	{
		public string method { get; set; }
		public object @params { get; set; }
	}

	[Serializable]
	internal sealed class RestResponseParams<TResult> : RestParamsBase
		where TResult : class, new()
	{
		public TResult result { get; set; }
	}

	#endregion RestParams
}
﻿using System;
using System.Linq.Expressions;
using AutoMapper;

namespace Exe.MyShows.Api.Extensions
{
	internal static class AutoMapperExtensions
	{
		static AutoMapperExtensions()
		{
			AutoMapperConfiguration.Configure();
		}

		public static TDestination AutoMap<TDestination>(this object source)
			where TDestination : class, new()
		{
			if (source == null)
				throw new ArgumentNullException(nameof(source));

			return source is TDestination ? (TDestination)source : Mapper.Map<TDestination>(source);
		}

		public static IMappingExpression<TSource, TDestination> MapMember<TSource, TDestination>(
			this IMappingExpression<TSource, TDestination> mapping,
			Expression<Func<TSource, object>> sourceMember,
			Expression<Func<TDestination, object>> destinationMember)

			where TSource : class, new()
			where TDestination : class, new()

			=> mapping?.ForMember(destinationMember, options => options.MapFrom(sourceMember));

		public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(
			this IMappingExpression<TSource, TDestination> mapping,
			Expression<Func<TDestination, object>> destinationMember)

			where TSource : class, new()
			where TDestination : class, new()

			=> mapping?.ForMember(destinationMember, options => options.Ignore());
	}
}
﻿using System;
using System.Net;
using RestSharp;

namespace Exe.MyShows.Api
{
	[Serializable]
	public class MyShowsException : Exception
	{
		public MyShowsException()
		{
		}

		public MyShowsException(string message) : base(message)
		{
		}

		public MyShowsException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}

	[Serializable]
	public class MyShowsBadRequestException : MyShowsException
	{
		public ResponseStatus Status { get; private set; }
		public HttpStatusCode StatusCode { get; private set; }

		public MyShowsBadRequestException(ResponseStatus status, HttpStatusCode statusCode, string message)
			: base(message)
		{
			Status = status;
			StatusCode = statusCode;
		}

		public MyShowsBadRequestException(ResponseStatus status, HttpStatusCode statusCode, string message, Exception innerException)
			: base(message, innerException)
		{
			Status = status;
			StatusCode = statusCode;
		}
	}

	[Serializable]
	public sealed class MyShowsUnauthorizedException : MyShowsBadRequestException
	{
		public MyShowsUnauthorizedException(ResponseStatus status, string message)
			: base(status, HttpStatusCode.Unauthorized, message)
		{
		}

		public MyShowsUnauthorizedException(ResponseStatus status, string message, Exception innerException)
			: base(status, HttpStatusCode.Unauthorized, message, innerException)
		{
		}
	}
}
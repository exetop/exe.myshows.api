﻿using AutoMapper;
using AutoMapper.Mappers;
using Exe.MyShows.Api.Extensions;
using Exe.MyShows.Api.Models;
using Exe.MyShows.Api.Models.Dto;

namespace Exe.MyShows.Api
{
	internal class AutoMapperConfiguration
	{
		public static void Configure()
		{
			Mapper.Initialize(config =>
			{
				config.AddConditionalObjectMapper().Where((s, d) => s.Name == $"{d.Name}Dto");

				config.SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
				config.DestinationMemberNamingConvention = new PascalCaseNamingConvention();

				config.CreateMap<TokenInfoDto, TokenInfo>()
					  .MapMember(s => s.expires_in, d => d.ExpiresInSeconds);

				config.CreateMap<ShowDto, Show>()
					  .MapMember(s => s.image, d => d.ImageUrl)
					  .MapMember(s => s.totalSeasons, d => d.SeasonsCount);

				config.CreateMap<EpisodeDto, Episode>()
					  .MapMember(s => s.image, d => d.ImageUrl)
					  .MapMember(s => s.episodeNumber, d => d.Number);
			});

			Mapper.Configuration.CompileMappings();
		}
	}
}